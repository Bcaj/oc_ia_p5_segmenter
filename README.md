# Projet 5 du parcours Ingénieur IA
Suivez ce lien et faite un truc <br>
https://www.kaggle.com/datasets/olistbr/brazilian-ecommerce/data <br>

J'exagère, il y a un référentiel de compétences et des conditions de travail à respecter :-) <br>
Le but à atteindre est de faire une <b>segmentation des clients</b> pour <b>fournir une description actionable</b> 
ainsi qu'un <b>maintenance</b> du modèle. <br><br>
Concrètement, il faut fournir :
- Analyse exploratoire (notebook)
- Essaies de différentes approches de modélisation (notebook, ou code commenté)
- Simulation pour déterminer la fréquence de mise à a jour du modèle (notebook)
- Support de présentation

# Données disponibles
Un schéma est disponible en image pour visualiser la relation des données.

# Profil de client 
Le but à atteindre pour le profil du client va être :
- client
- nombre de commandes passées
- À t'il eu une commande annulée
- à t'il passé une commande dans le mois en question
    - 12 colonnes, une pour chaque mois, s'il a passé plusieurs commande la ligne peut contenir plusieurs résultats
- délai moyen pour le client du temps de réception de son colis
- délai moyen du client entre le moment où il passe sa commande et il laisse un avis
- nombres d'article total que le client a commandé
- vrai ou faux pour chaque catégorie,
- montant total des articles
- prix des frais de transports
- le client a-t-il laissé une note de 1 2 3 4 5
- quel mode de paiement utilisé
- paiement echelonné ?
